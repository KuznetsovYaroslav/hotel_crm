package com.example.demo.dao.jdbc;

import com.example.demo.dao.HotelRoomRepository;
import com.example.demo.dao.entity.HotelRoom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;

@Repository
public class JdbcHotelRoomRepository implements HotelRoomRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public HotelRoom createHotelRoom(int number, int floor, int rooms, String quality, int bathrooms_number) {
        String insertQuery = "INSERT INTO hotel_rooms " +
                "(number, floor, rooms, quality, bathrooms_number) " +
                "values " +
                "(?, ?, ?, ?, ?)";

        Object[] params = new Object[] { number, floor, rooms, quality, bathrooms_number };
        int[] types = new int[] { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.VARCHAR, Types.INTEGER};

        int row =   jdbcTemplate.update(insertQuery, params, types);

        return null;
    }
}
