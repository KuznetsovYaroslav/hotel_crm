package com.example.demo.dao.entity;

public class HotelRoom {

    private int id;
    private int number;
    private int floor;
    private int rooms;
    private String quality;
    private int bathrooms_number;

    public HotelRoom(){}

    public HotelRoom(int id, int number, int floor, int rooms, String quality, int bathrooms_number){
        this.id = id;
        this.number = number;
        this.floor = floor;
        this.rooms = rooms;
        this.quality = quality;
        this.bathrooms_number = bathrooms_number;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public int getBathrooms_number() {
        return bathrooms_number;
    }

    public void setBathrooms_number(int bathrooms_number) {
        this.bathrooms_number = bathrooms_number;
    }
}
