package com.example.demo.dao;

import com.example.demo.dao.entity.User;

public interface UserRepository {
    public User getUser(String login, String password);
}
