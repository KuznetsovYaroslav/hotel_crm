package com.example.demo.dao;

import com.example.demo.dao.entity.HotelRoom;

public interface HotelRoomRepository {
    public HotelRoom createHotelRoom(int number, int floor, int rooms, String quality, int bathrooms_number);
}
