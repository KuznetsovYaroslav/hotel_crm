package com.example.demo.dao.jdbc;

import com.example.demo.dao.UserRepository;
import com.example.demo.dao.entity.User;
import com.example.demo.dao.mapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public class JdbcUserRepository implements UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public User getUser(String email, String password) {
        try{
            String sql = "SELECT * FROM USERS WHERE email = ? and password = ?";

            User user = (User) jdbcTemplate.queryForObject(sql, new Object[]{email, password}, new UserRowMapper());
            return user;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
