package com.example.demo.component;

import com.example.demo.dao.entity.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.ScopedProxyMode;

import java.time.LocalDateTime;

@Scope(value = "singleton")
@Component
public class SessionComponent {

    private User user;

    public SessionComponent() {
        System.out.println("DataSessionScope Constructor Called at "+ LocalDateTime.now());
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }

}
