package com.example.demo.presentation;

import com.example.demo.component.SessionComponent;
import com.example.demo.dao.entity.User;
import com.example.demo.domain.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.security.NoSuchAlgorithmException;

@ShellComponent
public class AuthController {
    @Autowired
    private AuthorizationService authorizationService;

    @Autowired
    private SessionComponent session;

    @ShellMethod("Authorize user")
    void authorize(String email, String pass) throws NoSuchAlgorithmException {
        User user = session.getUser();

        if( user != null ){
            System.out.println("You are already authorized.");
            return;
        }

        boolean isAuthorized = authorizationService.authUser(email, pass);

        if(isAuthorized){
             System.out.println("Welcome.");
             return;
        }

        System.out.println("User doesn't exist");
    }

    @ShellMethod("Logout user")
    void logout(){
        boolean wasLoggedIn = authorizationService.logoutUser();
        if(wasLoggedIn){
            System.out.println("You was successfully logged out.");
            return;
        }

        System.out.println("You was not authorized.");
    }
}
