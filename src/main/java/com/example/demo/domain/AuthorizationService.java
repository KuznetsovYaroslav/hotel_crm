package com.example.demo.domain;


import com.example.demo.component.SessionComponent;
import com.example.demo.dao.inmemory.InMemoryUserRepository;
import com.example.demo.dao.UserRepository;
import com.example.demo.dao.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.websocket.Session;

@Service
public class AuthorizationService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionComponent session;

    public boolean authUser(String email, String password) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(password.getBytes(),0,password.length());
        String md5Password = new BigInteger(1, m.digest()).toString(16);

        System.out.println(md5Password);
        User user = userRepository.getUser(email, md5Password);

        if (user != null) {
            session.setUser(user);
            return true;
        }

        return false;
    }

    public boolean logoutUser() {
        User user = session.getUser();
        if(user != null) {
            session.setUser(null);
            return true;
        }

        return false;
    }

}
