package com.example.demo.dao.inmemory;

import com.example.demo.dao.UserRepository;
import com.example.demo.dao.entity.User;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Map;

@Repository
public class InMemoryUserRepository implements UserRepository{
    private Map<String, User> users = null;

    @PostConstruct
    public void init() {
    }


    public User getUser(String login, String password){
        User user = this.users.get(login);

        if(user == null){
            return null;
        }

        if (user.getPassword().equals(password)) {
            return user;
        } else {
            return null;
        }
    }

}
